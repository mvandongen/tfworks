terraform {}

provider "aws" {}
provider "archive" {}

variable "name" {
  default = "custom_provider_example"
}

resource "aws_iam_role" "custom_provider" {
  name = var.name

  inline_policy {
    name = "inline_policy"

    policy = jsonencode({
      Version = "2012-10-17"
      Statement = [
        {
          Action   = [
            "logs:CreateLogGroup",
            "logs:CreateLogStream",
            "logs:PutLogEvents",
            "sns:CreateTopic",
            "sns:ListTopics",
            "sns:DeleteTopic"
          ]
          Effect   = "Allow"
          Resource = "*"
        }
      ]
    })
  }

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_lambda_layer_version" "custom_provider" {
  filename   = "lambda_layer/lambda_layer.zip"
  layer_name = var.name
  compatible_runtimes = ["python3.9"]
  source_code_hash = "${filebase64sha256("lambda_layer/lambda_layer.zip")}"
}

data "archive_file" "custom_provider" {
  type        = "zip"
  source_dir  = "lambda_function/"
  output_path = "lambda_function.zip"
}

resource "aws_lambda_function" "custom_provider" {
  filename         = data.archive_file.custom_provider.output_path
  function_name    = var.name
  source_code_hash = data.archive_file.custom_provider.output_base64sha256
  role             = aws_iam_role.custom_provider.arn
  handler          = "lambda.lambda_handler"
  runtime          = "python3.9"
  layers           = [aws_lambda_layer_version.custom_provider.arn]
  timeout          = 30
}

# # Uncomment if you want to execute this from other accounts
# resource "aws_lambda_permission" "custom_provider" {
#   statement_id  = "AllowExecutionFromOtherAccounts"
#   action        = "lambda:InvokeFunction"
#   function_name = aws_lambda_function.custom_provider.function_name
#   principal     = "cloudformation.amazonaws.com"
# }

output "lambda_function_arn" {
  value = aws_lambda_function.custom_provider.arn
}