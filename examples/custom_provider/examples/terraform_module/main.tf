provider "aws" {}
provider "archive" {}

data "archive_file" "lambda_zip" {
  type        = "zip"
  source_dir  = "module_src/"
  output_path = "tfmodule.zip"
}

resource "aws_s3_bucket" "bucket" {
  bucket = "mvd-tf-modules-public2"
}

resource "aws_s3_object" "data_zip" {
  bucket = aws_s3_bucket.bucket.id
  key    = data.archive_file.lambda_zip.output_path
  acl    = "public-read"
  source = data.archive_file.lambda_zip.output_path
  etag   = data.archive_file.lambda_zip.output_md5
}

output "url" {
  value = "https://${aws_s3_bucket.bucket.bucket_domain_name}/${aws_s3_object.data_zip.key}"
}
