provider "aws" {}
provider "random" {}

variable "name" {
  default = ""
}

resource "random_string" "random" {
  length  = 12
  upper   = false
  special = false
}

resource "aws_cloudformation_stack" "custom_sns_topic" {
  name = "crs-${random_string.random.result}"

  parameters = {
    TopicName = var.name
  }

  template_body = <<STACK
{
  "Parameters" : {
    "TopicName" : {
      "Type" : "String"
    }
  },
  "Resources" : {
    "CustomSNSTopic": {
      "Type" : "Custom::SNSTopic",
      "Properties" : {
        "ServiceToken" : "arn:aws:lambda:us-east-1:680242542234:function:custom_provider_example",
        "TopicName": { "Ref": "TopicName" }
      }
    }
  }
}
STACK
}