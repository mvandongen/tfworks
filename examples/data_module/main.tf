provider "aws" {}
provider "random" {}

module "data" {
  source = "./data"
}

output "ami_id" {
  value = module.data.ami_id
}