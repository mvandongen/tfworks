provider "aws" {}

module "network" {
  source = "./network"
  vpc_cidr_block = "10.100.0.0/16"
}