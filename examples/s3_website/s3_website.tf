terraform { }

variable "stack" {
	default = "mvdtfws-step-2"
}

# https://registry.terraform.io/providers/hashicorp/aws/3.75.2/docs/resources/s3_bucket

resource "aws_s3_bucket" "example" {
  bucket = var.stack
}

resource "aws_s3_bucket_website_configuration" "example" {
  bucket = aws_s3_bucket.example.id

  index_document {
    suffix = "index.html"
  }

  error_document {
    key = "error.html"
  }
}

resource "aws_s3_object" "index" {
  bucket = aws_s3_bucket.example.id
  key    = "index.html"
  content = templatefile("app/index.html.tftpl", {SOME_VARIABLE = "Terraform Developer"})
  acl    = "public-read"
  content_type = "text/html"
}

output "website" {
  value = aws_s3_bucket_website_configuration.example.website_endpoint
}