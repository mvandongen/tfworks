provider "aws" {}

module "custom_sns_topic" {
  source = "https://mvd-tf-modules-public2.s3.amazonaws.com/tfmodule.zip"

  name = "some_topic"
}