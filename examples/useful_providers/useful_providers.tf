provider "random" {}
provider "template" {}
provider "archive" {}

resource "random_string" "random" {
  length  = 8
  upper   = false
  special = false
}

data "http" "my_ip" {
  url = "http://ipv4.icanhazip.com"
}

data "archive_file" "zip" {
  type        = "zip"
  source_dir  = "./"
  output_path = "example.zip"
}

locals {
  my_ip = chomp(data.http.my_ip.body)
}

output "random" {
  value = random_string.random.result
}

output "my_ip" {
  value = local.my_ip
}