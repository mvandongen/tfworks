# terraform
terraform {}

# providers
provider "aws" {}
provider "archive" {}
provider "random" {}

resource "random_string" "random" {
  length           = 8
  special          = false
}

data "aws_caller_identity" "current" {}

data "archive_file" "lambda_zip" {
  type        = "zip"
  source_dir  = "lambda/"
  output_path = "lambda.zip"
}

resource "aws_lambda_function" "function" {
  function_name = random_string.random.result
  role          = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/AWSLambdaBasicExecutionRole"
  handler       = "lambda.lambda_handler"
  runtime       = "python3.9"
  filename      = data.archive_file.lambda_zip.output_path
  source_code_hash = data.archive_file.lambda_zip.output_base64sha256
}

resource "aws_lambda_function_url" "function" {
  function_name      = aws_lambda_function.function.function_name
  authorization_type = "NONE"
}

output "function_url" {
  value = aws_lambda_function_url.function.function_url
}