provider "aws" {}

variable "vpc_cidr_block" {
  default = "10.0.0.0/16"
}

variable "subnet_newbits" {
  default = 8
}

data "aws_availability_zones" "available" {
  state = "available"
}

resource "aws_vpc" "public" {
  cidr_block = var.vpc_cidr_block
}

resource "aws_internet_gateway" "igw" {
  vpc_id     = aws_vpc.public.id
}

resource "aws_subnet" "public" {
  for_each           = {for idx, az_name in data.aws_availability_zones.available.names: idx => az_name}
  vpc_id             = aws_vpc.public.id
  cidr_block         = cidrsubnet(aws_vpc.public.cidr_block, var.subnet_newbits, each.key)
  availability_zone  = data.aws_availability_zones.available.names[each.key]
}

resource "aws_route" "igw" {
  route_table_id          = aws_vpc.public.default_route_table_id
  destination_cidr_block  = "0.0.0.0/0"
  gateway_id              = aws_internet_gateway.igw.id
}