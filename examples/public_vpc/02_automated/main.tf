provider "aws" {}

data "aws_region" "current" {}

variable "vpc_cidr_block" {
  default = "10.0.0.0/16"
}

variable "subnet_newbits" {
  default = 8
}

locals {
  subnets = {
    0 = "a"
    1 = "b"
    2 = "c"
    3 = "d"
    4 = "e"
    5 = "f"
    6 = "g"
  }
}

resource "aws_vpc" "public" {
  cidr_block = var.vpc_cidr_block
}

resource "aws_internet_gateway" "igw" {
  vpc_id     = aws_vpc.public.id
}

resource "aws_subnet" "public" {
  for_each           = local.subnets
  vpc_id             = aws_vpc.public.id
  cidr_block         = cidrsubnet(aws_vpc.public.cidr_block, var.subnet_newbits, each.key)
  availability_zone  = "${data.aws_region.current.name}${each.value}"
}

resource "aws_route" "igw" {
  route_table_id          = aws_vpc.public.default_route_table_id
  destination_cidr_block  = "0.0.0.0/0"
  gateway_id              = aws_internet_gateway.igw.id
}