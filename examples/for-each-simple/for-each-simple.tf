provider "aws" {}

# get the default VPC
data "aws_vpc" "default" {
  default = true
}

# get subnets ids of default vpc
data "aws_subnets" "default" {
  filter {
    name   = "vpc-id"
    values = [data.aws_vpc.default.id]
  }
}

# get details of each subnet
data "aws_subnet" "subnet" {
  for_each = toset(data.aws_subnets.default.ids)
  id       = each.value
}

# create a route table for each subnet using for_each
resource "aws_route_table" "route_table" {
	for_each = data.aws_subnet.subnet
  vpc_id = data.aws_vpc.default.id

  tags = {
    AZ = each.value.availability_zone
  }
}