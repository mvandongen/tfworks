# Module 2: Upgrading & Troubleshooting

- Upgrading the Provider
- Using the Template Function
- Using more functions

Make sure your Terminal is in the workdir for module 2, type:

```text
cd ../../02/workdir
```

Again make sure your "yourname" and maybe other variables are updated! Do not change other things.

## Exercise 1: Upgrade Provider

Maybe you noticed in the previous exercise we used v3 of the AWS provider. AWS launched v4 earlier this year, and now we're going to do a small exercise to upgrade the provider.

Deploy the stack and notice the warning:

```text
│ Warning: Argument is deprecated
│
│   with aws_s3_bucket.example,
│   on s3.tf line 3, in resource "aws_s3_bucket" "example":
│    3: resource "aws_s3_bucket" "example" {
│
│ Use the aws_s3_bucket_website_configuration resource instead
│
│ (and 2 more similar warnings elsewhere)
```

Here is a guide that tells you how to upgrade. Although just updating your template and deploying works for this upgrade, let's take the official route! Most of the times, you'll run in complex issues.

[guide](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/guides/version-4-upgrade#website-website_domain-and-website_endpoint-arguments).

1. Update [main.tf](http://main.tf) line 10 version to `version = "~> 4.0"` and run `terraform init -upgrade`. (You can also change it back to 3.0 and run "upgrade" again to downgrade the provider.)
2. Make the changes as described on the page. (Hint: comment out website configuration as part of the aws_s3_bucket resource and add the **new** resource from the page.)
3. Run `terraform import aws_s3_bucket_website_configuration.example yourbucketname` (of course replace yourbucketname for the *actual* bucketname)

## Exercise 2: Use templating functions

Before we go into templating functions, let's first upload a file using the following code snippet. There is already a file in the app folder called index.html. So enter the path and file to the source, and also generate the etag using the **filemd5** function. Check out the example [here](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_object). And finally, find the output of the [s3 website url](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_website_configuration).

```hcl
resource "aws_s3_object" "index" {
  bucket = aws_s3_bucket.example.id
  key    = "index.html"
  source = ?
  etag   = ?
}

output "website" {
  value = ?
}
```

Now remove the lines of arguments `source` and `etag` and add the following line. Read the [documentation](https://www.terraform.io/language/functions/templatefile) to find out how to add a variable your template. In the tftpl file, you'll find a variable to replace. There is also one that should remain `${...}`, use the [docs](https://www.terraform.io/language/expressions/strings#string-templates) to fix that too.

```hcl
content = templatefile("path/to/file.tftpl", {})
```

## Exercise 3: Count

It's time for some magic. We'll use one resource block, to make multiple resources with one block in Terraform! Before we start, it's good to know for_each is the recommended option, but I just want to make sure you have touched count at least once.

Copy the existing aws_s3_object resource in your template, based on the below example rename the new one to "files". Then add `count` and alter the `key` argument.

When you deploy the template and open the bucket, you'll find 2 new files there.

```hcl
resource "aws_s3_object" "files" {
  count = 2

  bucket = aws_s3_bucket.example.id
  key    = "index${count.index}.html"

  # other arguments
}
```

## Exercise 4: Use more functions

Another great feature of Terraform is `terraform console`. Keep the stack deployed, and use this feature to experiment with [functions](https://www.terraform.io/language/functions). In the following exercises you'll see an example command on the first line, and (part of) the expected result on the rows below. If there are question marks in the command, you have to find out how to replace the ? for code to make it work.

What arguments and attributes are available for the resource: aws_s3_object.files ?

```text
> ?
{
  "acl" = "public-read"
  "bucket" = "mvdtfws-step-2"
  "bucket_key_enabled" = false
  ...
}
```

Because we used `count` to create multiple objects, we can use the following code to generate a list of file names (keys). Replace the question mark for `v.something`. Hint, just type `aws_s3_object.files` to see the list of objects.

```text
[for v in aws_s3_object.files : ?]
```

Use [cidrsubnet()](https://www.terraform.io/language/functions/cidrsubnet) to calculate a /24 network in a VPC cidr block: 10.0.0.0/16. First let's start with an example and then you try...

```text
> cidrsubnet("10.0.0.0/18", 4, 3)
"10.0.12.0/22"
> cidrsubnet(?, ?, ?)
"10.0.1.0/24"
```

Now make a list of cidr blocks for a list `["a", "b", "c"]`:

```text
> [for i, n in ? : cidrsubnet(?, ?, ?)]
[
  "10.0.0.0/24",
  "10.0.1.0/24",
  "10.0.2.0/24",
]
```

## Clean up

```text
terraform destroy -auto-approve
```

[Go to the Next Module](../03/)