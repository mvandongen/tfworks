terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

provider "aws" {
  default_tags {
    tags = {
			Owner = "yourname@company.net"
      Stack = var.stack
    }
  }
  region="eu-west-1"
}

variable "stack" {
	default = "yourname-step-2"
}

# https://registry.terraform.io/providers/hashicorp/aws/3.75.2/docs/resources/s3_bucket

resource "aws_s3_bucket" "example" {
  bucket = var.stack

  website {
    index_document = "index.html"
    error_document = "error.html"
  }
}