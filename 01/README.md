# Module 1

## Learning objectives

- Basic Terraform usage: templating language & CLI commands
- Multi-providers, -regions, -accounts
- Adding & linking resources
- Useful providers
- Some functions

## Exercise 1: Basics

> The challenge here is we know the recommended prerequisites and some basics of Terraform and now we want to get some hands-on experience without diving too deep into syntaxes and best practices. Just warming up for the rest of the workshop.

1. Open the tfworks project in VSCode and explore 01/workdir/main.tf, and find / replace "yourname" for your own name.
3. Run the following commands from the workdir folder and try to understand what happens:

```text
terraform init
terraform plan
terraform apply
terraform console
> aws_sns_topic.topic
press CTRL+D to exit the console
terraform show -json | jq
```

## Exercise 2: Multi-account / Multi-region

> The challenge here is to use terraform for multiple AWS accounts, regions, maybe even other cloud services.

Terraform supports many other providers, but you can configure "aws" only once. Unless you add an argument "alias" to the second provider. This way, you can have multiple aws providers in a single terraform project.

You might start with us-east-1 already, in that case use a different region (eu-west-1) or the other way around. If your sandbox account only allows us-east-1, just create the alias for the same region.

> Pay attention to dashes vs. underscores.

Example for region `eu-west-1`:

```hcl
provider "aws" {
  alias = "eu_west_1"
  region = "eu-west-1"
}

...
...

resource "aws_sns_topic" "topic_eu_west_1" {
  provider = aws.eu_west_1
}
```

Example for region `us-east-1`:
```hcl
provider "aws" {
  alias = "us_east_1"
  region = "us-east-1"
}

...
...

resource "aws_sns_topic" "topic_us_east_1" {
  provider = aws.us_east_1
}
```

Now apply again.

## Exercise 3: Import existing resources to terraform

> Sometimes you already created, configured and used an AWS service before you started with terraform. For example when you really needed a new feature, and there was no support yet for that feature in the resource. Once Terraform adds support for the feature, you want to import the resource so you can have 100% IaC covered again.

Create another SNS Topic (A standard one not FIFO) resource via the Management Console or CLI (NOT Terraform) and give it the name "sns_topic_tf_imported". Now we want to import the resource so we can manage it using Terraform. First note the ARN of the resource. Then add the resource to your template using the code snippet below.

```hcl
resource "aws_sns_topic" "topic_imported" {
  name = "sns_topic_tf_imported"
}
```

And then execute the [import](https://www.terraform.io/cli/commands/import) command. In this case an ARN is required, in other situations the import command might expect the ID of a resource.

```text
terraform import aws_sns_topic.topic_imported arn:aws:sns:us-east-1:123456789012:sns_topic_tf_imported
```

Now apply again.

## Exercise 4: Link resources

> So far all resources were just single resources, but most resources in AWS are linked. For example: a Load Balancer is linked to an EC2 AutoScalingGroup, a Subnet to a VPC, and a Route to a RouteTable. Let's find out how to link resources.

Let's add another resource to the template, and link the two. This is the architecture we would like to create. We only create the pink icons (sqs and sns) and the subscription. This is called a fan out architecture. In case of an event, for instance a file is uploaded to S3, it will send a message to sns and sns will forward that message to all subscribers, in this case only 1 sqs queue.

![](img/diagrams-sns-sqs.png)

1. Add resource: [aws_sqs_queue](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/sqs_queue), only with the arguments **name** and **tags**.
2. Add resource: [aws_sns_topic_subscription](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/sns_topic_subscription) with arguments **topic_arn**, **protocol**, and **endpoint**.

Hint 1: Replace the example topic_arn and endpoint for the arn of the resources.

```hcl
resource "aws_sns_topic_subscription" "sns_sqs_subscription" {
  topic_arn = "arn:aws:sns:us-east-1:123456789012:sns-topic"
  protocol  = "sqs"
  endpoint  = "arn:aws:sqs:us-east-1:123456789012:sqs-queue"
}
```

Hint 2: You can reference variables, resources and more using the following syntax. All arguments are available, the things you configured in terraform, but also additional attributes that are available **after creating the resources**. For example to get the arn of the SQS and SNS queue:

```text
aws_sns_topic.topic.arn
aws_sqs_queue.queue.arn
```

## Exercise 5: Other useful providers

> The provider "aws" is just there to create resources or to retrieve data from AWS. But terraform also has a lot of great features to generate random strings, for http requests, etc.

The hints for this block can be found in the [/examples/useful_providers/useful_providers.tf](/examples/useful_providers/useful_providers.tf). You'll learn the most by searching for the documentation and follow the instructions there. Links to documentation can be found in the instructions. If the documentation is unclear, you can also find information in the examples folder.

1. Use the random block to create a "random_name" ([random string](https://registry.terraform.io/providers/hashicorp/random/latest/docs/resources/string), [random provider](https://registry.terraform.io/providers/hashicorp/random/latest/docs))
2. Use http to find your public IP address: [http://checkip.amazonaws.com/](http://checkip.amazonaws.com/) ([http provider](https://registry.terraform.io/providers/hashicorp/http/latest/docs), [http data source](https://registry.terraform.io/providers/hashicorp/http/latest/docs/data-sources/http))
3. Use archive_file block to zip a file ([archive_file](https://registry.terraform.io/providers/hashicorp/archive/latest/docs/data-sources/archive_file)). You can use the some_folder/some_file.txt in the workdir.

## Exercise 6: Data block & outputs

> Actually, in that last exercise, we already used the a data block to get the IP address and to create the archive. Usually "resources" are definitions to *create* resources, while "data" are definitions to *get information* of existing resources. To me it still feels weird the archive is a data type, while we *create* a zip file, but OK... It is what it is.

A great use case for getting existing information, is: your [Account ID](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/caller_identity) or the latest [Amazon Linux 2 AMI](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/ami). Use the outputs feature of Terraform to show the results of both data blocks.

Hint, to get the right AMI, use the following arguments (yes, the syntax of this list is different on purpose!):

- most_recent = true
- owners = amazon
- filter: name = amzn2-ami-hvm-*-gp2
- filter: root-device-type = ebs
- filter: virtualization-type = hvm

To create an [output](https://www.terraform.io/language/values/outputs) use the following snippet.

```hcl
output "name_of_the_output" {
  value = some_resource.name.some_attribute
}
```

## Exercise 7: Shared State

> You probably noticed a few files appeared in your workdir. Folders like .terraform, and files like .terraform.tfstate. The .terraform is a temporary folder that includes all downloaded providers and modules, when you executed: terraform init. The terraform.tfstate holds your state. If you try to open the state file, you can't read it, but you can transform it to json. In this final exercise of this module, we're going to store the state in an S3 bucket.

First destroy your stack and cleanup your directory. Your workdir should only contain these files and folders:

```text
.
├── main.tf
└── some_folder
    └── some_file.txt
```

1. Manually create an S3 bucket in the us-east-1. Give it a unique name, because s3 bucket names are globally unique. "test" is already taken by someone, but maybe you can try: `ACCOUND-ID-tf`. It's recommended to enable versioning for the bucket!
2. Now add the following snippet to the top of your main.tf:

```hcl
terraform {
  backend "s3" {
    bucket = "add-your-own-bucket-here"
    key    = "terraform.tfstate"
    region = "us-east-1"
  }
}
```

Now deploy again, and there is no terraform.tfstate in the workdir anymore. It's in S3! Now others with access to the S3 and the source code, can also maintain the project. It's a best practice to store the state in S3, even if you have a "git" repo and CI/CD pipeline. Everytime terraform runs, the state will be refreshed. And sometimes, the state becomes corrupt. It's much easier to troubleshoot and fix the state file if you saved every version of it.

Throughout this workshop, we'll keep the state local unless explicitly mentioned.

## Clean up

Clean up our stack to get ready for the next module.

```text
terraform destroy -auto-approve
```

[Go to the Next Module](../02/)