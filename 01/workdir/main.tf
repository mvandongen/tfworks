terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

provider "aws" {
  default_tags {
    tags = {
      Owner = "yourname@company.net"
      Stack = var.stack
    }
  }
}

variable "stack" {
  default = "yourname-step-1"
}

variable "environment" {
  default = "sbx"
}

locals {
  prefix = "${var.stack}-${var.environment}"
  suffix = "20220622001"
}

resource "aws_sns_topic" "topic" {
  name = "${local.prefix}-topic-${local.suffix}"

  tags = {
    Name = "${local.prefix}-topic-${local.suffix}"
  }
}