# Module 4

* Advanced Modules
* Project structure for a large infrastructure
* Working with secrets (in Terraform)
* Working with secrets (in AWS)

Make sure your Terminal is in the workdir for module 4, type:

```text
cd ../../04/workdir
```

## Exercise 1: Advanced Modules

In the previous step, we created a module for our network as part of our project. But what if a team created a module and would like to share it with other teams, or maybe open source the module?

The `source` contained a local path in the previous example, but there is a list of [other options]. The top 5 in short:

- `"../modules/mymodule"`. A ./ or ../ indicates it's a local path.
- `"terraform-aws-modules/vpc/aws"`. If it begins with a namespace, like "terraform-aws-modules" or "hashicorp", the module is hosted on the [Terraform Registry](https://registry.terraform.io/)
- `"git@github.com..."` (ssh) or `"github.com/..."` (https). Gets the resource from GitHub.
- `git::ssh://username@example.com/storage.git` (ssh) or `"git::https://example.com/vpc.git` (https) from general git servers like GitLab.
- `https://example.com/module.zip` to host the module anywhere on the web.

> A special double-slash syntax is interpreted by Terraform to indicate that the remaining path after that point is a sub-directory within the package. For example: `terraform-aws-modules/vpc/aws` contains submodule for vpc-endpoints. You can use this module without using the complete module: `terraform-aws-modules/vpc/aws//modules/vpc-endpoints`. 

In the workdir you'll find a module, but the source is currently: `terraform-aws-modules/vpc/aws`. Try to change this to a source which starts with "github.com" and starting with "git::".

If you were able to init and plan the stacks, you can delete the main.tf.

## Exercise 2: Project Structure

For this exercise, I want to thank Anton for sharing [terraform best practices: large-size infrastructure](https://github.com/antonbabenko/terraform-best-practices/tree/master/examples/large-terraform). Let's migrate our "single main.tf file" to a project structure how it *should* be.

Create the following structure:

```text
.
├── dev
│   ├── main.tf
│   ├── outputs.tf
│   ├── terraform.tfvars
│   └── variables.tf
├── modules
│   └── network
│       └── main.tf
└── prod
    ├── main.tf
    ├── outputs.tf
    ├── terraform.tfvars
    └── variables.tf
```

1. Move the current `module "vpc" {} ` to modules/network/main.tf and make sure the network can be used for a dev, prod and multiple other VPCs (that should have a proper name and different cidr block)
2. Update the files **dev/main.tf** and **prod/main.tf** using the code block below (update the question mark in the terraform {} block to prod or dev)
3. Create an **outputs.tf** file in both folders (dev & prod) to output the vpc id.
3. Create an **variables.tf** (in dev & prod) to *define* all variables, and a **terraform.tfvars** to set these variables.

```hcl
terraform { }

provider "aws" {}

module "network" {
  source = "../modules/network"

  name = var.name

  cidr = var.cidr
  azs  = var.azs
  public_subnets = var.public_subnets
}

module "alb" {
  source = "terraform-aws-modules/alb/aws"
  version = "~> 6.0"
}
```

Deploy both the dev and prod stack and resolve any issues you encounter.

```hcl
module "http_sg" {
  source = "terraform-aws-modules/security-group/aws//modules/http-80"
  
  name        = "http-sg"
  description = "HTTP Open for the World"
  vpc_id      = ?
  ingress_cidr_blocks = ["0.0.0.0/0"]
}

module "alb" {
  source = "terraform-aws-modules/alb/aws"
  version = "~> 6.0"
  
  name = "my-alb"

  load_balancer_type = "application"

  vpc_id             = ?
  subnets            = ?
  security_groups    = [?]
  
  http_tcp_listeners = [
    {
      port        = 80
      protocol    = "HTTP"
      action_type = "fixed-response"
      fixed_response = {
        content_type = "text/plain"
        message_body = "Hello World"
        status_code = "200"
      }
    }
  ]
}
```

## Exercise 3: Secrets

Add the following variable to both **variables.tf** files:

```hcl
variable "some_secret" {
  description = "Some secret for our workload"
  type        = string
  default     = ""
  sensitive   = true
}
```

And in the command line where you type `terraform apply` etc, *first* execute:

```text
$ TF_VAR_some_secret=$(echo "S3cr3t!")
```

Create a Powershell or Bash script to set the secret variable and deploy the stack.

Take a look at the next example. We can store a secret in AWS's secretmanager and retrieve it. If you have time left, feel free to create a secret and retrieve it using the AWS CLI.

```text
$ TF_VAR_some_secret=$(aws secretsmanager get-secret-value --secret-id MySecret ...)
```

[Go to the Next Module](../05/)