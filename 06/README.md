# Module 6

Make sure your Terminal is in the workdir for module 6, type:

```text
cd ../../06/workdir
```

The project structure is already there. And here is a high level diagram of the workload:

![](img/diagram.png)

Make sure you configure:

1. A remote state on S3
2. A specific version of the AWS Provider (4.x)
3. Separate prod and dev modules
4. Create a network module and create your own resources (vpc, subnets)
5. An IAM Role, Security Group 
5. The lambda function, and optionally also a lambda layer
6. The lambda function url

Below some example code snippets you might want to use. Remember: you'll learn more when not using the examples!

```hcl

resource "aws_lambda_function" "function" {
  function_name = var.name
  role          = ?
  handler       = "lambda.lambda_handler"
  runtime       = "python3.9"
  filename      = ?
  source_code_hash = ?

  vpc_config {
    subnet_ids         = ?
    security_group_ids = [?]
  }
}

resource "aws_lambda_function_url" "function" {
  function_name      = ?
  authorization_type = "NONE"
}

resource "aws_security_group" "function" {
  name        = "allow_tls"
  vpc_id      = ?

  ingress {
    description      = "TLS from VPC"
    from_port        = 443
    to_port          = 443
    protocol         = "tcp"
    cidr_blocks      = [module.network.vpc_cidr_block]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
  }
}

resource "aws_iam_role" "iam_for_lambda" {
  name = var.name

  inline_policy {
    name = var.name

    policy = jsonencode({
      Version = "2012-10-17"
      Statement = [
        {
          Action   = [
            "ec2:CreateNetworkInterface",
            "ec2:DescribeNetworkInterfaces",
            "ec2:DeleteNetworkInterface",
            "ec2:DescribeSecurityGroups",
            "ec2:DescribeSubnets",
            "ec2:DescribeVpcs"
          ]
          Effect   = "Allow"
          Resource = "*"
        },
      ]
    })
  }

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}
```