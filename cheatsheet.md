# Terraform Cheatsheet

## Variable

```hcl
# define
variable "variable_name" {
	type = string
	default = "default_value"
}

# use
resource "resource_type" "resource_name" {
	option1 = var.variable_name
	option2 = "${var.variable_name}-more-text"
}
```

## Locals

```hcl
# define
locals = {
	variable_name = variable_value
}

# use
resource "resource_type" "resource_name" {
	option1 = local.variable_name
	option2 = "${local.variable_name}-more-text"
}
```

## Functions

```hcl
length(["a", "b", "c"])
```

## Resource

- "aws_instance” is the **type** of the resource
- "web” is the name in Terraform (this name is NOT visible in AWS)
- "ami” and "instance_type” are **arguments** for the resource (in CloudFormation these are called properties)
- You can re-use all arguments set (e.g. aws_instance.web.ami) and you can find additional attributes in the documentation, (e.g. [ec2_instance](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/instance#attributes-reference))

```hcl
# Creating a resource
resource "aws_instance" "web" {
  ami           = "ami-a1b2c3d4"
  instance_type = "t2.micro"
}

# Referencing a resource argument or attribute
output "public_dns" {
	value = aws_instance.web.public_dns
}
```

## Data

[See example](../examples/data/data.tf)

```hcl
# define a data object
data "aws_ami" "amazon_linux_2" {
  most_recent = true

  filter {
    name   = "owner-alias"
    values = ["amazon"]
  }

  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-*-x86_64-ebs"]
  }
}

# Using the result of the data block
resource "aws_instance" "web" {
  ami           = data.aws_ami.amazon_linux_2.image_id
  instance_type = "t2.micro"
}
```

## For_each

[See example](../examples/for-each-simple/for-each-simple.tf)

```hcl
# get the default VPC
data "aws_vpc" "default" {
  default = true
}

# get subnets ids
data "aws_subnets" "default" {
  filter {
    name   = "vpc-id"
    values = [data.aws_vpc.default.id]
  }
}

# get details of each subnet
data "aws_subnet" "subnet" {
  for_each = toset(data.aws_subnets.default.ids)
  id       = each.value
}

# create route table for each subnet using for_each
resource "aws_route_table" "route_table" {
	for_each = data.aws_subnet.subnet
  vpc_id   = data.aws_vpc.default.id

  tags = {
    AZ = each.value.availability_zone
  }
}
```