# Terraform Workshop

## Prerequisites

### Using a hosted IDE & Sandbox

1. Login using the credentials provided
2. Choose Region: **eu-west-1 (Ireland)**
3. Go to the **Cloud9** Service
4. Create a new instance using the following information:

Name: **Copy your Username** (from the e-mail) and click next
Environment Type: **Create new EC2 instance for environment** (already selected)
Instance Type: Other instance type: **t3.micro**
Platform: **Amazon Linux 2** (already selected)
Cost-saving setting: **After 30 minutes** (already selected)
VPC: **default VPC** (already selected)

5. Login to you Cloud9 IDE (automatically), using Chrome or Firefox
6. If a Terminal is not opened, click **Window** and **Terminal** and execute the following script (or one by one):

```text
sudo yum -y install terraform
git clone https://gitlab.com/mvandongen/tfworks.git
cd tfworks/01/workdir
```

### Using your own Laptop & Sandbox

- AWS Account
- [VSCode](https://code.visualstudio.com/download)
- [Terraform](https://www.terraform.io/downloads)
- [AWS CLI v2](https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html) (Warning: [Uninstall V1](https://docs.aws.amazon.com/cli/v1/userguide/cli-chap-install.html) first if you have it!)

## Windows Users

Throughout the workshop we'll use Mac/Linux examples only. If you're working with Windows, use Powershell and the following alternative commands:

|Description|Mac/Linux|Windows|
|---|---|---|
|Set env var AWS_REGION|`export AWS_REGION=us-east-1`|`$env:AWS_REGION='us-east-1'`|
|The path to a user folder|`~/.aws`|`"$($env:USERPROFILE)\.aws"`|

## Other Remarks

* Naming things is hard. In Terraform, you have to give your resources a logical name. You could keep them short, or long, specific or grouped. I have an opinion, but just do what feels right for today.
* Terraform: snake_case. When naming things, I prefer snake_case. When you see me using "CamelCase" or "kebab-case" it's either an error, or snake_case cannot be used.
* Many things in IT are case-sensitive.
* There is no **perfect** project structure for Terraform. Some prefer a single file (main.tf) some prefer many files (provider.tf, terraform.tf, s3.tf etc) and others limit it to ~4. In this workshop we start with a single file, and try out other project structures throughout the workshop.

## Install VSCode

* [Download and Install VSCode](https://code.visualstudio.com/download)
* It's highly recommend to install these extensions (click on the icon with 4 blocks on the left). Search exactly for these names:
  - AWS Toolkit
  - HashiCorp Terraform (by HashiCorp)
  - indent-rainbow
  - Markdown Preview Enhanced
  - vscode-icons
  - YAML (by Red Hat)

## Download this repo

Make copy of this repository:

https://gitlab.com/mvandongen/tfworks

Clone the repo. (or download the zip if you're not familiar with Git)

```text
$ cd /tmp
$ git clone https://gitlab.com/mvandongen/tfworks.git
```

Open a folder: tfworks/01/workdir in VSCode:

![](img/vscode.png)

## Using AWS's Game Engine

1. Login to the game engine
2. Open the workdir of the module in VSCode and open a new Terminal
3. Copy the CLI credentials to the Terminal / Powershell Terminal
4. Set the default region (`export AWS_REGION=us-east-1` or `$env:AWS_REGION='us-east-1'`)

## FAQ

### Did I set my credentials correctly?

Type `aws s3 ls` and it should show your s3 buckets. An empty response is great, you probably don't have buckets yet.

Type `env | grep AWS` on a Linux/Mac or `Get-ChildItem -Path Env:` on a Windows Powershell computer.

[Go to Module 1](./01/)