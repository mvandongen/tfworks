# Module 3

- Troubleshooting and fixing common problems
- Using for_each in various ways
- First time creating a module

Make sure your Terminal is in the workdir for module 3, type:

```text
cd ../../03/workdir
```

## Exercise 1: Troubleshooting

Deploy the main.tf in the workdir without editting. The bug is there on purpose. If you deploy the template, it will raise this error:

```text
│ Error: error creating EC2 Subnet: InvalidSubnet.Conflict: The CIDR '10.0.0.0/24' conflicts with another subnet
│       status code: 400, request id: 06af1e0b-6767-4c55-9a74-5b933c7983df
│ 
│   with aws_subnet.a,
│   on public_vpc.tf line 11, in resource "aws_subnet" "a":
│   11: resource "aws_subnet" "a" {

│ Error: error creating EC2 Subnet: InvalidSubnet.Conflict: The CIDR '10.0.0.0/24' conflicts with another subnet
│       status code: 400, request id: 1a1ff337-d710-4ec6-8381-9c03f0ac4667
│ 
│   with aws_subnet.c,
│   on public_vpc.tf line 21, in resource "aws_subnet" "c":
│   21: resource "aws_subnet" "c" {
```

In our example we know 'b' was deployed successfully, but 'a' and 'c' not. The error is clear about the problem, so let's try to solve it. If we change the cidr_block of 'b' to 10.0.1.0/24 and 'c' to 10.0.2.0/24 and apply the template, it will sometimes raise the same error. (Don't try it, because 33% chance it will work... So please trust me: you will run into this problem some time, so better learn how to resolve).

Of course we can just destroy the whole stack, but what if creating the stack took 1 hour? Let's just destroy the specific resource, and then apply again.

```text
terraform destroy -target RESOURCE_TYPE.NAME
```

## Exercise 2: for_each

Terraform is so called "deterministic". But what is that? "In computer science, a deterministic algorithm is an algorithm that, given a particular input, will always produce the same output, with the underlying machine always passing through the same sequence of states." ([Source](https://en.wikipedia.org/wiki/Deterministic_algorithm)).

But if you look at the template, 3 resources look almost the same: aws_subnet. They also have a lot of hard coded values for the cidr block. Let's assume we would like to use this template for many workloads, and every workload needs to have a different cidr block for the subnet, and the VPCs.

We're using for_each and some other functions we learned before, to create these VPCs.

First add the following code snippet somewhere to the main.tf:

```hcl
variable "vpc_cidr_block" {
  default = "10.0.0.0/16"
}

variable "subnet_newbits" {
  default = 8
}

locals {
  subnets = {
    0 = "a"
    1 = "b"
    2 = "c"
    3 = "d"
    4 = "e"
    5 = "f"
    6 = "g"
  }
}
```

Now remove the aws_subnets a, b, and c resource blocks (about 12 lines of code) and add the following code snippet instead. Then: replace the question marks. You may need the internet, or use the hints below the code block.

```hcl
resource "aws_subnet" "public" {
  for_each           = ?
  vpc_id             = aws_vpc.public.id
  cidr_block         = cidrsubnet(aws_vpc.public.cidr_block, ?, ?)
  availability_zone  = ?
}
```

Hints:

1. `cidrsubnet(aws_vpc.public.cidr_block, 8, 2)` returns "10.0.2.0/24".
2. If a for_each is defined, a `each.key` and `each.value` is available.
3. You can combine variables using `"${var.var_a}${var_b}"`. If var_a = us-east-1 and var_b = c, the result is: "us-east-1c".

## Exercise 3: for_each

Now remove the `locals` block from the main.tf and use the following example snippets. Add the data aws_availability_zones block, and replace the aws_subnet block for the following. Deploy it and continue after the code block...

```hcl
data "aws_availability_zones" "available" {
  state = "available"
}

resource "aws_subnet" "public" {
  for_each           = data.aws_availability_zones.available.names
  vpc_id             = aws_vpc.public.id
  cidr_block         = cidrsubnet(aws_vpc.public.cidr_block, var.subnet_newbits, each.key)
  availability_zone  = data.aws_availability_zones.available.names[each.key]
}
```

It will raise an error... try to solve it yourself by adding some logic to the for_each field. Here is a [stackoverflow question](https://stackoverflow.com/questions/63309824/for-each-availability-zone-within-an-aws-region#answers) that should help. Hint: only change the for_each line. In the examples/public-vpc/03-clean are more "hints" if you really can't figure it out.

## Exercise 4: Creating our network module

Modules are a great way to organize your project, to share ready-to-use solutions to teams and communities. Let's quickly see it in action.

Create a folder "network" in the workdir and create a file in that folder: main.tf. Now cut & paste all blocks to the network/main.tf, except the `terraform {}` and `provider {}` blocks.

Your new ./main.tf will look like this, and add the module block too:

```hcl
terraform {}

provider "aws" {}

module "network" {
  source = "./network"

  vpc_cidr_block = "10.100.0.0/16"
}
```

Now run `terraform init`, to initialize the module, and run terraform plan. Not much changed in the plan, but now we have a nice structure for our network.

[Go to the Next Module](../04/)