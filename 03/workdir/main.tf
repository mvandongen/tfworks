terraform {}

provider "aws" {}

resource "aws_vpc" "public" {
  cidr_block = "10.0.0.0/16"
}

resource "aws_internet_gateway" "igw" {
  vpc_id     = aws_vpc.public.id
}

resource "aws_subnet" "a" {
  vpc_id     = aws_vpc.public.id
  cidr_block = "10.0.0.0/24"
  availability_zone = "us-east-1a"
}

resource "aws_subnet" "b" {
  vpc_id     = aws_vpc.public.id
  cidr_block = "10.0.0.0/24"
  availability_zone = "us-east-1b"
}

resource "aws_subnet" "c" {
  vpc_id     = aws_vpc.public.id
  cidr_block = "10.0.0.0/24"
  availability_zone = "us-east-1c"
}

resource "aws_route" "igw" {
  route_table_id          = aws_vpc.public.default_route_table_id
  destination_cidr_block  = "0.0.0.0/0"
  gateway_id              = aws_internet_gateway.igw.id
}