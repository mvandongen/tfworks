provider "aws" {}
provider "random" {}

variable "name" {
  default = ""
}

resource "random_string" "random" {
  length  = 12
  upper   = false
  special = false
}

resource "aws_cloudformation_stack" "custom_sns_topic" {
  name = "crs-${random_string.random.result}"

  parameters = {
    TopicName = var.name
  }

  template_body = <<STACK
{
  "Parameters" : {
    "TopicName" : {
      "Type" : "String"
    }
  },
  "Resources" : {
    "CustomSNSTopic": {
      "Type" : "Custom::SNSTopic",
      "Properties" : {
        "ServiceToken" : "arn-of-the-custom-provider",
        "TopicName": { "Ref": "TopicName" }
      }
    }
  }
}
STACK
}