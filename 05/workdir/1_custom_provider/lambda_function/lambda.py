import boto3
import botocore.exceptions
from crhelper import CfnResource

client = boto3.client('sns')
helper = CfnResource()

@helper.create
def create(event, _):
  if 'TopicName' in event['ResourceProperties']:
    try:
      response = client.create_topic(Name=event['ResourceProperties']['TopicName'])    
    except botocore.exceptions.ClientError as error:
      raise ValueError("Unable to create topic: " + error)
  else:
    raise ValueError("TopicName is a required property.")
  
  helper.Data["TopicArn"] = response['TopicArn']
  return response['TopicArn']

@helper.update
def update(event, _):
  return True

@helper.delete
def delete(event, _):
  try:
    response = client.delete_topic(TopicArn=event['PhysicalResourceId'])
  except botocore.exceptions.ClientError as error:
    pass

def lambda_handler(event, context):
  helper(event, context)