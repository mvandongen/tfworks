# Module 5

- Custom providers
- Publishing modules
- Using Modules to deploy custom providers

Make sure your Terminal is in the workdir for module 5, type:

```text
cd ../../05/workdir
```

If you're a go developer, you can write your own "provider" to extend Terraform. Not everyone *is* comfortable in go, and fortunately, CloudFormation has a really nice "custom resource provider". We will learn immediately how to make it easy for others to use your custom provider. This module consists of 4 steps:

1. Create and deploy a custom_provider
2. Create a module
3. Publish the module on an https address
4. Use the custom provider with the module

## Exercise 1: Create and deploy a custom_provider

In the workdir I already created a lambda_layer/lambda_layer.zip. This zip file contains 2 additional libraries: requests and crhelper. Crhelper is required for custom providers for CloudFormation. I have added requests for advanced users who might want to update the lambda function (lambda.py). If you have Python installed, feel free to "rebuild" the lambda_layer.zip. 

If you open main.tf, there are already 50+ files present. A Lambda function requires an IAM role to create and delete resources, and also to write logs and metrics to CloudWatch. Because this is a Terraform workshop and not an AWS deep dive in IAM, it's created for for you.

1. Create a [zip file](https://registry.terraform.io/providers/hashicorp/archive/latest/docs/data-sources/archive_file) of the `lambda_function/` and the output is: `lambda_function.zip`.
2. Create a [aws_lambda_layer_version](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lambda_layer_version). Runtime: python3.9. Besides the filename pointing to the lambda_layer.zip, you also need to calculate the source_code_hash using the function `filebase64sha256()`.
3. Create a [aws_lambda_function](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lambda_function). Checklist for your arguments: filename, function_name, source_code_hash, role, handler, runtime, layers, timeout (30). 

Tip: data.archive_file.custom_provider has an output_path (pointing to the zip) and also calculates the source_code_hash in output_base64sha256.

Do not forget to **output** the function arn, because you need it in the next exercise.

> Too easy? If you could use a bit more challenge... Update the lambda.py and IAM permissions, so you're not creating an SNS Topic, but instead you create an SQS Queue, or S3 Bucket.

Are you working with your own AWS Account? Use this code:

```hcl
resource "aws_iam_role" "custom_provider" {
  name = var.name

  inline_policy {
    name = "inline_policy"

    policy = jsonencode({
      Version = "2012-10-17"
      Statement = [
        {
          Action   = [
            "logs:CreateLogGroup",
            "logs:CreateLogStream",
            "logs:PutLogEvents",
            "sns:CreateTopic",
            "sns:ListTopics",
            "sns:DeleteTopic"
          ]
          Effect   = "Allow"
          Resource = "*"
        }
      ]
    })
  }

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}
```

## Exercise 2: Create a module

Go to the folder **workdir/2_module/module_src/** and open the main.tf, update the `ServiceToken`, which is the function arn of the previous exercise. You can test the deployment with a temporary variable name set, but do not forget to cleanup the module_src folder so only main.tf remains there.

## Exercise 3: Publish the module on an https address

Let's now create a module (some zipped terraform files), create a public website (s3) and upload the zip to that s3 bucket. The output, an https address, is used in the custom provider.

1. Use [archive_file](https://registry.terraform.io/providers/hashicorp/archive/latest/docs/data-sources/archive_file) to create a zip file of **module_src**.
2. Create an [aws_s3_bucket](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket). Tip; only set a name for the bucket.
3. Create an [aws_s3_object](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_object). Hints: Remember from previous exercises you can use output_md5 of the archive file to fill the etag. Just like key and source can use output_path from the archive...
4. And an output that shows the https address to the s3 bucket and the object would be great. 

<p>
<details>
<summary>Still struggling with step 4?</summary>
<pre>
"https://${aws_s3_bucket.bucket.bucket_domain_name}/${aws_s3_object.data_zip.key}"
</pre>
</details>
</p>

## Exercise 4: Use the custom provider with the module

Open folder 4_workload. Now the only thing we have to do here is open the main.tf and add a module with source the path of exercise 3, and one argument: TopicName: some_name_you_like.

## Exercise 5: Cleanup

Now cleanup in the correct order:

1. First destroy exercise 4_workload
2. Then destroy exercise 2_module
3. And finally destroy 1_custom_provider

[Go to the Next Module](../06/)